# Proyecto reto

#Diagrama de clases
Esta compuesto de 3 clases, una para los proyectos, de la cual tiene atributos individuales, y dos referencias; luego la segunda es de los miembros del proyecto con atributos individuales, un embebido para la direccion y una lista de compuesta de dos partes, lenguajes y nivel, y finalmente la de las historias que esta compuesta de solamente atributos individuales.

#Diagrama de actividad
Describe el proceso que necesita el SCRUM desde el backlog incluyendo los release de cada uno, y también la retrospectiva para cada release del como haya resultado la user story, para al final hacer el cierre de proyecto.

#Link de docker hub
https://hub.docker.com/repository/docker/danterodriguez/proyecto_reto

